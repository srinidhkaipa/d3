Workspace -

Composer - Layout - GrapesJS - Provides the layout attributes and theme attributes
  
 _ Everything is component ( even page is also consider as component)
_ Router Config is special attribute to Component  
 \* Grouping components

Orchestrator - Flow designer - GG Editor / MX graph
  
 _ Understand components and Services
_ Integrates the components to Services

Provider - Services - GG Editor / MX graph

    * Entitty driven Service definitions
    * Aggregrates

( State management / Observability )
Changes in properties validates the publish the changes to other components

Exception handling

Console - WARNING / ERROR

Schema

- JSONNET - JSON data templating
- ajv
- jsonata

NeDB - stores the workspace data

Javascript / Typescript - Parser - Functions - Parameters - Return - Imports - Dependencies - Inside function calls - References - AST graph
