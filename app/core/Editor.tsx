/* eslint-disable react/prop-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-types */
import React, { PropsWithChildren } from 'react';
import GrapesJS, { Editor as IEditor } from 'grapesjs';

export interface EditorProps {
  id: string;

  plugins?: string[];

  storageManager?: any;

  blockManager?: any;

  styleManager?: {};

  width?: string | number;

  height?: string | number;

  components?: object[];

  blocks?: object[];

  panels?: {};

  onInit?(editor: IEditor): void;

  onDestroy?(editor: IEditor): void;
}

const Editor = React.forwardRef<IEditor | null, PropsWithChildren<EditorProps>>(
  (
    {
      id,
      blockManager,
      styleManager,
      storageManager,
      width,
      height,
      plugins,
      panels,
      onInit,
      onDestroy,
    },
    ref
  ) => {
    const [editor, setEditor] = React.useState<IEditor | null>(null);

    React.useEffect(() => {
      // eslint-disable-next-line no-shadow
      const editor = GrapesJS.init({
        container: `#${id}`,
        fromElement: true,
        blockManager,
        styleManager,
        storageManager,
        width,
        height,
        plugins,
        panels,
      });
      if (typeof onInit === 'function') {
        onInit(editor);
      }
      setEditor(editor);

      return function cleanup() {
        if (editor) {
          if (typeof onDestroy === 'function') {
            onDestroy(editor);
          }
          GrapesJS.editors = GrapesJS.editors.filter((e: any) => e !== editor);
          editor.destroy();
          if (document) {
            const container: HTMLDivElement = document.getElementById(
              id
            ) as HTMLDivElement;
            if (container) {
              container.innerHTML = '';
            }
          }
        }
      };
    }, [
      blockManager,
      height,
      id,
      onDestroy,
      onInit,
      plugins,
      storageManager,
      styleManager,
      width,
      panels,
    ]);

    React.useImperativeHandle(ref, () => {
      return editor;
    });

    return <div id={id} />;
  }
);

Editor.defaultProps = {
  id: 'grapesjs-react-editor',
  plugins: [],
  blocks: [],
  blockManager: {},
  storageManager: {},
  styleManager: {},
  panels: {},
  width: 'auto',
  height: '100vh',
  components: [],
};

export default Editor;

(window as any).GrapesJS = GrapesJS;
