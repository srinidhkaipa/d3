/* eslint-disable no-console */
import React from 'react';
import { ipcRenderer } from 'electron';
import grapesjsPluginToolbox from 'grapesjs-plugin-toolbox';
import grapesjsBlocksFlexbox from 'grapesjs-blocks-flexbox';
import myPlugin from './blocks/d3-grapesjs-plugin.min.js';
import Editor from '../../core/Editor';
import './Grape.css';

export default function Grape() {
  return (
    <>
      <Editor
        id="gjs"
        plugins={[grapesjsPluginToolbox, grapesjsBlocksFlexbox, myPlugin]}
        onInit={(editor) => {
          editor.Panels.addButton('options', {
            id: 'generateCode',
            label: `<i class="fa fa-code" aria-hidden="true"></i>`,
            attributes: { title: 'Generate Code' },
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            command: (e: any) => {
              const input = {
                Components: JSON.parse(JSON.stringify(e.getComponents())),
                Css: JSON.parse(JSON.stringify(e.getCss())),
                Wrapper: JSON.parse(JSON.stringify(e.getWrapper())),
                Style: JSON.parse(JSON.stringify(e.getStyle())),
                Html: JSON.parse(JSON.stringify(e.getHtml())),
              };
              console.log(input)
              ipcRenderer
                .invoke('open-preview', input)
                .then((result) => {
                  console.log(result);
                  return null;
                })
                .catch((err) => {
                  console.error(err);
                });
            },
          });

          editor.Panels.removeButton('options', 'fullscreen');
          editor.Panels.removeButton('options', 'export-template');
        }}
      />
    </>
  );
}
