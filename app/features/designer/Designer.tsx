import React from 'react';
import { Layout, Input } from 'antd';
import SplitPane from 'react-split-pane';
import Pane from 'react-split-pane/lib/Pane';
import ComponentDesignerSidenav from '../../components/component-designer-sidenav/ComponentDesignerSidenav';
import DragArea from '../../components/DragArea/DragArea';
import './Designer.css';

const { Content, Sider } = Layout;
const { Search } = Input;

function Designer() {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <SplitPane split="vertical">
        <Pane minSize="256px" initialSize="256px" maxSize="50%">
          <Sider
            className="item-sider"
            width="100%"
            theme="light"
            style={{
              overflow: 'auto',
              height: '100vh',
              left: 0,
            }}
          >
            <Search
              placeholder="search component"
              // eslint-disable-next-line no-console
              onSearch={(value) => console.log(value)}
              style={{ width: '80%', margin: '20px' }}
            />
            <ComponentDesignerSidenav />
          </Sider>
        </Pane>
        <Pane>
          <Layout className="site-layout">
            <Content style={{ margin: '0 16px' }}>
              <DragArea />
            </Content>
          </Layout>
        </Pane>
      </SplitPane>
    </Layout>
  );
}

export default Designer;
