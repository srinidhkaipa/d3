import React from 'react';
import { Collapse, List } from 'antd';

import {
  FormOutlined,
  BarChartOutlined,
  AppstoreOutlined,
  DiffOutlined,
} from '@ant-design/icons';
import Box from '../Box/Box';

const FormComponents = [
  'Input',
  'Select',
  'Radio',
  'Checkbox',
  'Date Picker',
  'Time Picker',
];
const EssentialComponents = [
  'Container',
  'layout 12',
  'layout 6,6',
  'layout 3,9',
  'layout 9,3',
  'layout 3,6,3',
  'layout 4,4,4',
  'layout 3,3,3,3',
  'layout 2,2,2,2,2,2',
];
const { Panel } = Collapse;

const formTitle = (
  <div>
    <FormOutlined />
    <span>&nbsp;Form Inputs</span>
  </div>
);
const essentialsTitle = (
  <div>
    <DiffOutlined />
    <span>&nbsp;Essentials</span>
  </div>
);
const tableTitle = (
  <div>
    <AppstoreOutlined />
    <span>&nbsp;Tables</span>
  </div>
);
const chartTitle = (
  <div>
    <BarChartOutlined />
    <span>&nbsp;Charts</span>
  </div>
);

export default function ComponentDesignerSidenav() {
  return (
    <Collapse bordered={false} expandIconPosition="right" ghost>
      <Panel header={essentialsTitle} key="1">
        <List
          dataSource={EssentialComponents}
          renderItem={(item) => (
            <List.Item>
              <Box name={item} />
            </List.Item>
          )}
        />
      </Panel>
      <Panel header={formTitle} key="2">
        <List
          dataSource={FormComponents}
          renderItem={(item) => (
            <List.Item>
              <Box name={item} />
            </List.Item>
          )}
        />
      </Panel>
      <Panel header={tableTitle} key="3">
        <List
          dataSource={FormComponents}
          renderItem={(item) => (
            <List.Item>
              <Box name={item} />
            </List.Item>
          )}
        />
      </Panel>
      <Panel header={chartTitle} key="4">
        <List
          dataSource={FormComponents}
          renderItem={(item) => (
            <List.Item>
              <Box name={item} />
            </List.Item>
          )}
        />
      </Panel>
    </Collapse>
  );
}
