import React, { useState } from 'react';
import { useDrop } from 'react-dnd';
import { List } from 'antd';
import update from 'immutability-helper';

import ItemTypes from '../../utils/items';
import DragItem from '../DragItem/DragItem';

const style: React.CSSProperties = {
  height: '512px',
  textAlign: 'center',
};
interface BoxProps {
  id: number;
  name: string;
}
let countId = 0;

export default function DragArea() {
  const [formItems, setFormItems] = useState<BoxProps[]>([]);
  const findFormItem = (id: number) => {
    const item = formItems.filter((c) => c.id === id)[0];
    return {
      item,
      index: formItems.indexOf(item),
    };
  };

  const moveFormItems = (id: number, atIndex: number) => {
    const { item, index } = findFormItem(id);

    setFormItems(
      update(formItems, {
        $splice: [
          [index, 1],
          [atIndex, 0, item],
        ],
      })
    );
  };

  const [{ canDrop, isOver }, drop] = useDrop({
    accept: ItemTypes.FORM,
    drop: (item: {
      type: string;
      name: string;
      id?: number;
      index?: number;
    }) => {
      if (!item.id) {
        countId += 1;
        setFormItems([...formItems, { name: item.name, id: countId }]);
      }
    },

    collect: (monitor) => {
      return { isOver: monitor.isOver(), canDrop: monitor.canDrop() };
    },
  });

  const isActive = canDrop && isOver;
  const backgroundColor = '#ffffff';

  const removeItem = (id: number) => {
    setFormItems(formItems.filter((i) => i.id !== id));
  };

  return (
    <div ref={drop} style={{ ...style, backgroundColor }}>
      {isActive ? 'Release to drop' : 'Form Container'}
      <List
        bordered
        dataSource={formItems}
        locale={{ emptyText: 'No Items' }}
        renderItem={(item) => (
          <List.Item>
            <DragItem
              name={item.name}
              id={item.id}
              removeItem={removeItem}
              moveFormItems={moveFormItems}
              findFormItem={findFormItem}
            />
          </List.Item>
        )}
      />
    </div>
  );
}
