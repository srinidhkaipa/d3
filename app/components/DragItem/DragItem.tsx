import React from 'react';
import { DeleteOutlined } from '@ant-design/icons';
import { useDrag, useDrop } from 'react-dnd';

import ItemTypes from '../../utils/items';

interface BoxProps {
  id: number;
  name: string;
}
interface DragItemProps {
  id: number;
  name: string;
  removeItem: (id: number) => void;
  moveFormItems: (dragIndex: number, hoverIndex: number) => void;
  findFormItem: (id: number) => { item: BoxProps; index: number };
}
interface Item {
  type: string;
  id: number;
  originalIndex: number;
}
const style = {
  width: '100%',
};
export default function DragItem({
  name,
  id,
  removeItem,
  moveFormItems,
  findFormItem,
}: DragItemProps) {
  const originalIndex = findFormItem(id).index;
  const [{ isDragging }, drag] = useDrag({
    item: { type: ItemTypes.FORM, id, originalIndex },
    collect: (monitor) => {
      return { isDragging: monitor.isDragging() };
    },
    end: (_dropResult, monitor) => {
      const { id: droppedId, originalIndex: index } = monitor.getItem();

      const didDrop = monitor.didDrop();
      if (!didDrop) {
        moveFormItems(droppedId, index);
      }
    },
  });

  const [, drop] = useDrop({
    accept: ItemTypes.FORM,
    canDrop: () => false,
    hover({ id: draggedId = -1 }: Item) {
      if (draggedId !== id && draggedId !== -1) {
        const { index: overIndex } = findFormItem(id);
        moveFormItems(draggedId, overIndex);
      }
    },
  });
  const opacity = 1;
  return (
    <div ref={(node) => drag(drop(node))} style={{ ...style, opacity }}>
      {name}
      <DeleteOutlined
        onClick={() => {
          removeItem(id);
        }}
      />
    </div>
  );
}
