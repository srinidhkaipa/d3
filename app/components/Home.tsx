import React from 'react';
import { Link } from 'react-router-dom';
import routes from '../constants/routes';
import styles from './Home.css';

export default function Home(): JSX.Element {
  return (
    <div className={styles.container} data-tid="container">
      <h2>Home</h2>
      <Link to={routes.COUNTER}>Counter</Link>
      <br />
      <Link to={routes.DESIGNER}>Designer</Link>
      <br />
      <Link to={routes.D3}>D3</Link>
    </div>
  );
}
