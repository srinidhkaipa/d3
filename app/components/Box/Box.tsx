import React from 'react';
import { useDrag, DragSourceMonitor } from 'react-dnd';
import ItemTypes from '../../utils/items';

import './Box.css';

const style: React.CSSProperties = {
  cursor: 'move',
  width: '100%',
  height: '100%',
};

interface BoxProps {
  name: string;
}

export default function Box({ name }: BoxProps) {
  const [{ isDragging }, drag] = useDrag({
    item: {
      name,
      type: ItemTypes.FORM,
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  const opacity = isDragging ? 0.4 : 1;

  return (
    <div ref={drag} className="box" style={{ ...style, opacity }}>
      {name}
    </div>
  );
}
