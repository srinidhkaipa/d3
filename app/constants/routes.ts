const routes = {
  HOME: '/',
  COUNTER: '/counter',
  DESIGNER: '/designer',
  D3: '/d3',
};
export default routes;
