/* eslint-disable no-console */
import { app, ipcMain } from 'electron';
import { join } from 'path';
import { generateCode, processCodeInput } from './core/generate';
import { generateFile } from './utils';

ipcMain.handle('open-preview', async (_event, templateInput) => {
  const templatePath = join(app.getPath('userData'), 'plugins', 'react');
  const workspace = join(app.getPath('userData'), 'reactApp', 'myapp');
  const { taskOutputQueue, utils, data } = processCodeInput(
    templatePath,
    templateInput,
    workspace
  );
  const out = [
    ...generateCode(
      join(templatePath, 'template'),
      { templateInput, utils, data },
      workspace,
      []
    ),
    ...taskOutputQueue,
  ];
  generateFile(out);
  return out;
});
