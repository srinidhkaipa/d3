import { exec } from 'child_process';
import { write } from 'fs-jetpack';
// eslint-disable-next-line import/prefer-default-export
export function generateFile(files: any) {
  files.forEach((file: any) => {
    write(file.outputPath, file.outputContent);
  });
}

export function installApp(path: string, cmd: string) {
  return new Promise((resolve, reject) => {
    const p = exec(cmd, { cwd: path }, (err, stdout, stderr) => {
      if (err) reject(err);
      resolve({ stdout, stderr, p });
    });
  });
}
