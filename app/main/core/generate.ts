/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-console */
import { readdirSync, statSync, readFileSync, existsSync } from 'fs';
import { join, normalize } from 'path';
import { render } from 'ejs';
import _ from 'lodash';

const getOutputPath = (rawPath: any, input: any) => {
  const p = render(
    rawPath,
    { ...input },
    {
      openDelimiter: '{',
      closeDelimiter: '}',
      delimiter: '_',
    }
  );

  return normalize(p.trim());
};

const getContent = (
  originalContent: string,
  input: any,
  outputPath: string
) => {
  try {
    const content = render(originalContent, { ...input });
    return content;
  } catch (error) {
    throw new Error(`${error} at ${outputPath}`);
  }
};

export function generateCode(
  templatePath: string,
  templateInput: any,
  workspace: string,
  queue: []
) {
  const filesToCreate = readdirSync(templatePath);
  filesToCreate.forEach((file) => {
    const origFilePath = join(templatePath, file);
    const stats = statSync(origFilePath);
    if (stats.isFile()) {
      const templateContent = readFileSync(origFilePath, 'utf8');
      const outputPath: string = getOutputPath(
        join(workspace, file),
        templateInput
      );
      const outputContent: string = getContent(
        templateContent,
        templateInput,
        outputPath
      );
      queue.push({ outputPath, outputContent });
    } else {
      generateCode(origFilePath, templateInput, join(workspace, file), queue);
    }
  });
  return queue;
}

export function processCodeInput(
  templatePath: string,
  templateInput: any,
  workspace: string
) {
  const manifestPath = join(templatePath, 'manifest.json');
  const manifestData = JSON.parse(readFileSync(manifestPath, 'utf8'));

  const utilsPath = join(templatePath, manifestData.main || 'index.js');
  const tasksPath = join(
    templatePath,
    manifestData.tasksConfig || 'tasks.json'
  );
  const preProcessPath = join(
    templatePath,
    manifestData.pre || 'process/pre.js'
  );

  // import all utils
  let utilsImport = {};
  if (existsSync(utilsPath)) {
    utilsImport = require(utilsPath);
  }
  // get all tasks
  let tasks: any = {};
  if (existsSync(tasksPath)) {
    tasks = JSON.parse(readFileSync(tasksPath, { encoding: 'utf8' }));
  }
  // start pre processing
  let preProcessExports = {};
  let data = {};
  if (existsSync(preProcessPath)) {
    try {
      preProcessExports = require(preProcessPath).run({
        templateInput,
        tasks,
      });
      tasks = preProcessExports.tasks;
      data = preProcessExports.data;
    } catch (error) {
      throw new Error(`pre generation error:${error}`);
    }
  }
  console.log(tasks);

  // task Generation
  // eslint-disable-next-line prefer-const
  let taskOutputQueue: any[] = [];
  if (Object.keys(tasks).length > 0) {
    Object.keys(tasks).forEach((t: any) => {
      let taskInput;
      if (typeof tasks[t] === 'string') {
        taskInput = _.get(templateInput, tasks[t]);
      } else if (Array.isArray(tasks[t])) {
        taskInput = tasks[t];
      } else {
        throw new Error(
          `error:${t} task input should be type of array or string`
        );
      }
      if (taskInput.length > 0) {
        taskInput.forEach((i: any) => {
          const taskTemplatePath = join(templatePath, 'tasks', t);
          if (!existsSync(taskTemplatePath)) {
            throw new Error(`${taskTemplatePath} does not exits`);
          }

          taskOutputQueue.push(
            ...generateCode(
              taskTemplatePath,
              {
                ...i,
                templateInput,
                data,
                utils: { utilsImport, _ },
              },
              workspace,
              []
            )
          );
        });
      }
    });
  }
  return { taskOutputQueue, data, utils: { utilsImport, _ } };
}
